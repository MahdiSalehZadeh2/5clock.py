
import matplotlib.pyplot as plt 
from math import *
import datetime#ساعت
import matplotlib.animation as animation

cxy = 20
circle_r = 20
big_r = 16
small_r = 6


fig = plt.figure()
ax = fig.add_subplot(111)

def calculate():
  now = datetime.datetime.now()
  m = now.minute#دقیقه
  h = now.hour# ساعت
  s= now.second#ثانیه
  deg_s = (15-s) *6
  xs = [cxy , cxy + (big_r * cos(radians(deg_s)))]
  ys = [cxy , cxy + (big_r * sin(radians(deg_s)))]




  deg_m = (15-m) *6
  xm = [cxy , cxy + (big_r * cos(radians(deg_m)))]
  ym = [cxy , cxy + (big_r * sin(radians(deg_m)))]


  deg_h = ((3 - (h % 4) ) * 3) - (m / 2)
  xh = [cxy , cxy + (deg_h * cos(radians(deg_h)))]

  yh = [cxy , cxy + (deg_h * sin(radians(deg_h)))]
 
  return [[xh , yh] , [xm , ym] , [xs , ys]]
#--------------------


def calor(i):


  res = calculate()

  ax.clear()
  ax.plot(res[2][0] , res[2][1] , color = 'r' , linestyle = '--')#s
  ax.plot(res[1][0] , res[1][1] , color = 'b' , linestyle = '--')#m
  ax.plot(res[0][0] , res[0][1] , color = 'y' )#h


  ax.add_artist(plt.Circle((cxy,cxy) , circle_r , fill=False))

  ax.set_aspect("equal")
  ax.axis([0,40,0,40])



#===============================




anime = animation.FuncAnimation(fig ,calor , interval=1000)# تکرار

plt.show()
